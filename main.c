///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   // Sum array1[] with a for() loop and print the result
   for(i = 0, i < 100, i++){
      sum1 = sum1 + array1[i];
      printf(sum);
   };

   // Sum array2[] with a while() { } loop and print the result
   while(i < 100, i++){
      sum2 = sum2 + array2[i];
      printf(sum2,"\n");
   }

   // Sum array3[] with a do { } while() loop and print the result
   do{
      sum3 = sum3 + array3[i];
   
      printf(sum3,"\n");
   }while(i < 100, i++);
	return 0;
}

